//
//  FiveWeatherData.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import Foundation


// MARK: - Welcome
struct Welcome: Codable {
    let list: [Lists]?
}

// MARK: - List
struct Lists: Codable {
    let dt: Int?
    let main: MainClass?
    let weather: [Weather]?
}

// MARK: - MainClass
struct MainClass: Codable {
    let temp: Double?
    let humidity: Int?
    let pressure: Int?
    
    
}

// MARK: - Weather
struct Weather: Codable {
    let main: MainEnum?
}

enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case snow = "Snow"
    case rain = "Rain"
}



