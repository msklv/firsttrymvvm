//
//  WeatherDataRealm.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 22.03.21.
//

import Foundation
import RealmSwift


class WeatherDataRealm: Object {
    
    @objc dynamic var visibility: Int = 0 
    @objc dynamic var main: Mainx?
    @objc dynamic var wind: Windx?
    @objc dynamic var name: String = ""
    @objc dynamic var weather: Weatherr?
    @objc dynamic var id: String = UUID().uuidString
    override static func primaryKey() -> String? {
        #keyPath(id)
    }
}

class Weatherr: Object {
    @objc dynamic var main: String = ""
}

class Mainx: Object {
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var feels_like: Double = 0.0
    @objc dynamic var pressure: Int = 0
    @objc dynamic var humidity: Int = 0
    @objc dynamic var id: String = UUID().uuidString
    
    override static func primaryKey() -> String? {
        #keyPath(id)
    }
}

class Windx: Object {
    @objc dynamic var speed: Double = 0.0
    @objc dynamic var id: String = UUID().uuidString
    
    override static func primaryKey() -> String? {
        #keyPath(id)
    }
}

