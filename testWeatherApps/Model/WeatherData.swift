//
//  WeatherModel.swift
//  WeatherNewApp
//
//  Created by Павел Москалёв on 28.02.21.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation


struct WeatherData: Codable {
    let weather: [Weathers]?
    let main: Mains?
    let wind: Winds?
    let name: String?
    let visibility: Int?
}



struct Mains: Codable {
    let temp: Double?
    let feels_like: Double?
    let pressure: Int?
    let humidity: Int?
}

struct Weathers: Codable {
    let main: String?
    
    enum main: String, Codable {
        case clear = "Clear"
        case clouds = "Clouds"
        case rain = "Rain"
        case snow = "Snow"
        case thunderstorm = "ThunderStorm"
        case mist = "Mist"
        case drizzle = "Drizzle"
    }
}

struct Winds: Codable {
    let speed: Double?
}

