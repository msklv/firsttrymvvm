//
//  NetworkManager.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 15.03.21.
//

import Foundation
import RealmSwift


class NetworkManager {
    
    func fetchWeather(longitude : String, latitude : String ,complition : @escaping(WeatherData?) -> Void){
        if let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=19f4ab338a14a90e3d170eaeaa5f6383&units=metric") {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
                guard let self = self else { return }
                if let data = data {
                    let weatherData = try? JSONDecoder().decode(WeatherData.self, from: data)
                    self.saveToRealm(weatherData: weatherData!)
                    complition(weatherData)
                } else {
                    DispatchQueue.main.async {
                        print("NO INTERNET!!!!!!!")
                    }
                }
                if let error = error {
                    print(error)
                }
                if let response = response {
                    print(response)
                }
            }
            task.resume()
        }
    }
    
    func fetch(longitude : String, latitude : String ,_ completionHandler: @escaping(Welcome?) -> Void) {
        if let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?lat=\(latitude)&lon=\(longitude)&appid=19f4ab338a14a90e3d170eaeaa5f6383&units=metric&lang=ru") {
            print(url)
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = data {
                    let objects = try! JSONDecoder().decode(Welcome.self, from: data)
                    completionHandler(objects)
                }
            }
            task.resume()
        }
    }
    func saveToRealm(weatherData: WeatherData) {
        do {
            let realm = try Realm()
            try realm.write() {
                realm.deleteAll()
            }
        } catch {
            print(error)
        }
        updateToRealm(weatherData: weatherData)
    }
    
    func updateToRealm(weatherData:WeatherData) {
        let weatherDataRealm = WeatherDataRealm()
       
        
        
        weatherDataRealm.name = weatherData.name!
        weatherDataRealm.visibility = weatherData.visibility ?? 0
        let weather = Weatherr()
        weather.main = weatherData.weather?[0].main ?? ""
        weatherDataRealm.weather = weather
        let mains = Mainx()
        mains.temp = weatherData.main?.temp ?? 1
        mains.feels_like = weatherData.main?.feels_like ?? 1.0
        mains.pressure = weatherData.main?.pressure ?? 1
        mains.humidity = weatherData.main?.humidity ?? 1
        weatherDataRealm.main = mains
        let winds = Windx()
        winds.speed = weatherData.wind?.speed ?? 1.0
        weatherDataRealm.wind = winds
        do {
            let realm = try Realm()
            try realm.write() {
                realm.add(weatherDataRealm)
            }
        } catch {
            print(error)
        }
    }
}
