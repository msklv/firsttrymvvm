//
//  Cordinates.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import Foundation


class Coordinates {
    var lat: String?
    var lon: String?
    
    
    static let shared = Coordinates()
    
    private init() {}
    
}
