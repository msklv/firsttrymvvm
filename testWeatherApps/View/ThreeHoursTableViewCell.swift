//
//  ThreeHoursTableViewCell.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import UIKit

class ThreeHoursTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionImage: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    weak var viewModel: ViewModelCell? {
        willSet(viewModel) {
            if let weather = viewModel {
                let dataCell = weather.dt
                let date = Date(timeIntervalSince1970: Double(dataCell ?? 1))
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                let dayOfWeekString = dateFormatter.string(from: date)
                self.timeLabel.text = "\(dayOfWeekString)"
                self.descriptionLabel.text = String( (weather.description ?? "" ))
                self.tempLabel.text = String(format: "%.0f",weather.temp ?? 0) + " °C"
                switch weather.description {
                case "Clouds": descriptionImage.image = UIImage(named: "clouds")
                case "Clear": descriptionImage.image = UIImage(named: "sunny")
                case "Snow": descriptionImage.image = UIImage(named: "snow")
                case "Rain": descriptionImage.image = UIImage(named: "rain")
                case .none:
                    break
                case .some(_):
                    break
                }
            }
        }
    }
    
    static let nib = "ThreeHoursTableViewCell"
    static let identifier = "tableCell"
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}




