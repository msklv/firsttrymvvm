//
//  CurrentViewController.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 14.03.21.
//

import UIKit
import CoreLocation
import RealmSwift

class CurrentViewController: UIViewController{
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var prepareView: UIView!
    @IBOutlet weak var descriptionImage: UIImageView!
    @IBOutlet weak var nameCityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityLogo: UIImageView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureImage: UIImageView!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var speedWindImage: UIImageView!
    @IBOutlet weak var speedWindLabel: UILabel!
    @IBOutlet weak var tempNowImage: UIImageView!
    @IBOutlet weak var tempNowLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    
    var viewModel = ViewModel()
    var weekViewModel = FiveDaysViewController()
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    let geoCoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    @IBAction func ShareButton(_ sender: Any) {
        let shareController = UIActivityViewController(activityItems: ["Hey! now i'm in \(viewModel.city) city. we have \(viewModel.description), temperature right now \(viewModel.tempNow)°C "],applicationActivities: nil)
        present(shareController, animated: true , completion: nil)
    }
    func fetchWeather(longitude: String, latitude: String) {
        viewModel.fetchWeather(longitude: longitude, latitude: latitude) {
            DispatchQueue.main.async {
                self.nameCityLabel.text = self.viewModel.data?.name ?? ""
                self.tempLabel.text = String(format: "%.0f",self.viewModel.data?.main?.temp ?? 0 ) + "°C" + " | " + ((self.viewModel.data?.weather?[0].main) ?? "") 
                self.humidityLabel.text = String(self.viewModel.data?.main?.humidity ?? 0 ) + "%"
                self.pressureLabel.text = String(self.viewModel.data?.main?.pressure ?? 0 ) + "hPa"
                self.tempNowLabel.text = String(self.viewModel.data?.main?.feels_like ?? 0 ) + "°C"
                self.speedWindLabel.text = String(self.viewModel.data?.wind?.speed ?? 0 ) + "m/s"
                self.visibilityLabel.text = String(self.viewModel.data?.visibility ?? 0 ) + "m"
                let someWeatherData = self.viewModel.data?.weather?[0]
                switch someWeatherData?.main {
                case "Clear":
                    self.descriptionImage.image = UIImage(named: "sunny")
                case "Snow":
                    self.descriptionImage.image = UIImage(named: "snow")
                case "Rain":
                    self.descriptionImage.image = UIImage(named: "rain")
                case "Clouds":
                    self.descriptionImage.image = UIImage(named: "clouds")
                default:
                    break
                }
                UIView.animate(withDuration: 0.5) {
                    self.prepareView.alpha = 0
                }
                
            }
        }
    }
}

extension CurrentViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        geoCoder.reverseGeocodeLocation(location) { (placemarks, _) in
            if placemarks != nil{
                placemarks?.forEach({ (placemark) in
                    if let city = placemark.locality {
                        self.viewModel.fetchCoordinate(longitude: "\(location.coordinate.longitude)", latitude: "\(location.coordinate.latitude)")
                        self.fetchWeather(longitude: self.viewModel.longitude, latitude: self.viewModel.latitude)
                        Coordinates.shared.lat = "\(location.coordinate.latitude)"
                        Coordinates.shared.lon = "\(location.coordinate.longitude)"
                    }
                })
            } else {
                let cancelAlert = UIAlertController(title: "Connection lost", message: "A 5-day forecast will be displayed if internet is available. You can view the last saved request for the current weather", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default) { [weak self] (_) in
                    let weatherDataRealm = WeatherDataRealm()
                    do {
                        let realm = try Realm()
                        let realmWeather = Array(realm.objects(WeatherDataRealm.self))
                        
                        self?.nameCityLabel.text = realmWeather.first?.name ?? ""
                        
                        self?.humidityLabel.text = "\(String(realmWeather.first?.main?.humidity ?? 0)) %"
                        self?.pressureLabel.text = "\(String(realmWeather.first?.main?.pressure ?? 0)) hPa"
                        self?.tempNowLabel.text = "\(String(realmWeather.first?.main?.feels_like ?? 0.0)) °C"
                        self?.speedWindLabel.text = "\(String(realmWeather.first?.wind?.speed ?? 0 )) m/s"
                        self?.visibilityLabel.text = "\(String(realmWeather.first?.visibility ?? 0 )) m"
                        self?.tempLabel.text = "\(String(format: "%.0f", realmWeather.first?.main?.temp ?? 0.0 )) °C | \(String(realmWeather.first?.weather?.main ?? ""))"
                    
                        let someWeatherData = realmWeather.first?.weather?.main ?? ""
                        switch someWeatherData {
                        case "Clear":
                            self?.descriptionImage.image = UIImage(named: "sunny")
                        case "Snow":
                            self?.descriptionImage.image = UIImage(named: "snow")
                        case "Rain":
                            self?.descriptionImage.image = UIImage(named: "rain")
                        case "Clouds":
                            self?.descriptionImage.image = UIImage(named: "clouds")
                        default:
                            break
                        }
                    }
                    catch {
                        print(error)
                    }
                    self?.shareButton.isHidden = true
                    self?.tabBarController?.tabBar.isHidden = true
                    UIView.animate(withDuration: 0.5) {
                        self?.prepareView.alpha = 0
                    }
                }
                cancelAlert.addAction(action)
                self.present(cancelAlert, animated: true, completion: nil)
            }
        }
    }
}
