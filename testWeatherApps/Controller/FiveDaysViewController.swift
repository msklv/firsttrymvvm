//
//  ViewController.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 13.03.21.
//

import UIKit

class FiveDaysViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {
    
    var viewModel: TableViewModel?
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            let nib = UINib(nibName: ThreeHoursTableViewCell.nib, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: ThreeHoursTableViewCell.identifier)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TableViewModel()
        viewModel?.fetchData({
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(for: section) ?? 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ThreeHoursTableViewCell.identifier, for: indexPath) as! ThreeHoursTableViewCell
        let cellViewModel = viewModel?.cellViewModel(indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSection() ?? 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.titleForHeaderInSection(section)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(viewModel?.heightForSectionAtRows() ?? 1)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "detail") as! InDetailViewController
        if let  unique = viewModel?.unique {
            controller.pressure = viewModel?.weatherForDays[unique[indexPath.section]]?[indexPath.row].main?.pressure
        controller.humidity = viewModel?.weatherForDays[unique[indexPath.section]]?[indexPath.row].main?.humidity
        controller.data = viewModel?.weatherForDays[unique[indexPath.section]]?[indexPath.row].dt
        }
        present(controller, animated: true, completion: nil)
    }
}





