//
//  InDetailViewController.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 29.03.21.
//

import UIKit

class InDetailViewController: UIViewController {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    
    var pressure: Int?
    var humidity: Int?
    var data: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pressure = pressure {
            pressureLabel.text = "\(String(describing: pressure)) hPa"
        }
        if let humidity = humidity {
            humidityLabel.text = "\(String(describing: humidity)) %"
        }
        if let data = data {
            let days = data
            let date = Date(timeIntervalSince1970: Double(days))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMMM | EEEE | YYYY"
            let dayOfWeekString = dateFormatter.string(from: date)
            dateFormatter.dateFormat = "h:mm a"
            let hourOfDayString = dateFormatter.string(from: date)
            dayLabel.text = dayOfWeekString
            hourLabel.text = hourOfDayString
        }
    }
}
