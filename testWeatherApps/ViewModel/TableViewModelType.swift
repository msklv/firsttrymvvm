//
//  TableViewModelType.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import Foundation

protocol TableViewModelType {
    
    func cellViewModel(_ indexPath: IndexPath) -> ViewModelCell?
    func numberOfSection() -> Int
    func numberOfRowsInSection(for section: Int) -> Int
    func fetchData(_ completionHandler: @escaping() -> ())
    func titleForHeaderInSection(_ section: Int) -> String
    func heightForSectionAtRows() -> Int
}
