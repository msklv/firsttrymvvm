//
//  TableViewModel.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import Foundation
class TableViewModel: TableViewModelType {
    
    var list: [Lists]?
    var network = NetworkManager()
    var days: [String] = []
    var weatherForDays: [String : [Lists]] = [:]
    var unique: [String] = []
    
    func fetchData(_ completionHandler: @escaping () -> ()) {
        guard let long = Coordinates.shared.lon, let lat = Coordinates.shared.lat else { return }
        network.fetch(longitude: long, latitude: lat) { (welcome) in
            self.list = welcome?.list
            if let list = self.list {
                for index in 0..<list.count {
                    let dataCell = welcome?.list?[index].dt
                    let date = Date(timeIntervalSince1970: Double(dataCell ?? 1))
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEEE"
                    let dayOfWeekString = dateFormatter.string(from: date)
                    if self.weatherForDays[dayOfWeekString] == nil {
                        self.weatherForDays[dayOfWeekString] = []
                    }
                    self.weatherForDays[dayOfWeekString]?.append((welcome?.list?[index]) as! Lists )
                    self.days.append(dayOfWeekString)
                }
                self.unique = self.days
                    .enumerated()
                    .filter{ self.days.firstIndex(of: $0.1) == $0.0 }
                    .map{ $0.1 }
                completionHandler()
            }
        }
    }
    func numberOfSection() -> Int {
        return unique.count
    }
    func titleForHeaderInSection(_ section: Int) -> String {
        return unique[section]
    }
    func cellViewModel(_ indexPath: IndexPath) -> ViewModelCell? {
        let data = weatherForDays[unique[indexPath.section]]?[indexPath.row]
        return ViewModelCell(data: data)
    }
    func numberOfRowsInSection(for section: Int) -> Int {
        weatherForDays[unique[section]]?.count ?? 0
    }
    func heightForSectionAtRows() -> Int {
        return 50
    }
}






