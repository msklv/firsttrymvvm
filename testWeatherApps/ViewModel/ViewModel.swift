//
//  ViewModel.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 16.03.21.
//

import Foundation
class ViewModel {
    
    var networkManager =  NetworkManager()
    var data: WeatherData?
    var longitude = ""
    var latitude = ""
    var city = ""
    var description = ""
    var tempNow = 0.0
    
    func fetchCoordinate(longitude: String,latitude: String) {
        self.longitude = longitude
        self.latitude = latitude
    }
    
    func fetchWeather(longitude: String, latitude: String, completion: @escaping() ->()) {
        networkManager.fetchWeather(longitude: longitude, latitude: latitude) { [weak self] weather in
            self?.data = weather
            self?.city = weather?.name ?? ""
            self?.tempNow = weather?.main?.feels_like ?? 0.0
            self?.description = weather?.weather?[0].main ?? ""
            completion()
        }
    }
}

