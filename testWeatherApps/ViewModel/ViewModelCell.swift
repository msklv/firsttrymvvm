//
//  ViewModelCell.swift
//  testWeatherApps
//
//  Created by Павел Москалёв on 17.03.21.
//

import Foundation
class ViewModelCell {
    
    var data: Lists?
    var temp: Double? {
        return data?.main?.temp
    }
    var dt: Int? {
        return data?.dt
    }
    var description: String? {
        return data?.weather?[0].main?.rawValue
    }
    
    init(data: Lists?) {
        self.data = data
    }
    
}
